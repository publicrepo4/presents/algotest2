﻿#include <bits/stdc++.h>
#include <unordered_map>
#include <windows.h>

// Функція для знаходження останніх повторень символів в рядку
void badCharHeuristic(std::string str, int size, std::unordered_map<char, int>& badchar)
{
    // За відповідним кодом ASCII символа записуємо його індекс, 
    // якщо символ повториться то він перезапише значення минулого, тим самим лишиться тільки позиція останнього.
    for (int i = 0; i < size; i++)
        badchar[str[i]] = i;
}

/* Функція для пошуку патерну в текста з використанням Поганої символьної евристики
алгоритма Бойера-Мура*/
std::vector<int> BoyerMooreSearchBadHeuristic(std::string txt, std::string pat)
{
    std::vector<int> occurrences;

    int m = pat.size();
    int n = txt.size();

    std::unordered_map<char, int> badchar;
    /* Знайдемо всі останні входження символа в рядок*/
    badCharHeuristic(pat, m, badchar);

    int s = 0; //зміщення в тексті

    while (s <= (n - m))
    {
        // Ініціалізація початку патерна
        int j = m - 1;

        /* Продовжуємо зменшувати j допоки не закінчиться патерн та
        символ паттерна в індексі j збігається з відповідним символом в самому тексті*/
        while (j >= 0 && pat[j] == txt[s + j])
            j--;

        /* Якщо рядок збігається за даним зміщенням, то j стане -1 */
        if (j < 0)
        {
            /* Знайдено підрядок! Додаємо його в список повторів */
            occurrences.push_back(s);

            /* Зсуваємось до наступного можливого варіанту входження */

            if (s + m >= n) {
                s++;
                break;
            }

            if (badchar.find(txt[s + m]) != badchar.end()) {
                s += m - badchar[txt[s + m]];
            }
            else { // якщо такого символу немає в підрядку, то здвигаємось на m + 1;
                s += m + 1;
            }
        }

        else
            /* Зсуваємо патерн так, щоб символ з тексту який не співпадає,
            співпадав з останнім повторенням цього символу у патерні*/
            if (badchar.find(txt[s + m]) != badchar.end()) { 
                s += max(1, j - badchar[txt[s + j]]);
            }
            else {
                s += j + 1;
            }
    }

    return occurrences;
}
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    while (true) {
        std::cout << "Введіть рядок в якому буде відбуватися пошук:\n";
        std::string txt;
        std::getline(std::cin, txt);

        std::cout << "Введіть пошуковий запит (Пробіли теж враховуються):\n";

        std::string pat;
        std::getline(std::cin, pat);

        std::cout << "Пошук в тексті " << pat << std::endl;

        std::vector<int> occurrences = BoyerMooreSearchBadHeuristic(txt, pat);

        std::cout << "\n\nРезультат: ";

        for (int index : occurrences)
            std::cout << "Знайдено підрядок на індексі " << index << std::endl;


        std::cout << "Пояснення: \n";
        for (int index : occurrences) {
            std::string cpy(txt.begin(), txt.end());

            for (int i = 0; i < pat.size(); i++)
                cpy[i + index] = '_';

            std::cout << cpy << std::endl;

            for (int i = 0; i < index; i++)
                std::cout << " ";

            std::cout << "^ " << pat << std::endl;
        }
    }
}

